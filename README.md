# IZS meter


## Getting started
passwd svestkajede
```

# Update package list
sudo apt update

# Update the package list and install the RTL-SDR library
sudo apt-get update
sudo apt-get install -y rtl-sdr

# Create the rtl-sdr.rules file in the /etc/udev/rules.d directory
sudo nano /etc/udev/rules.d/rtl-sdr.rules

# Add the following lines to the file:
# RTL-SDR
SUBSYSTEM=="usb", ATTRS{idVendor}=="0bda", ATTRS{idProduct}=="2838", MODE:="0666"

# Save the file and exit the editor

# Reload the udev rules
sudo udevadm control --reload-rules
sudo udevadm trigger


# Download izs-meter
git clone https://gitlab.com/PanFiluta/izs-meter.git

```



## Usage
```
cd izs-meter
python3 monitor.py
```
Instructions for calibrating the police detector:

Run the "monitor.py" script to start the detector. It will create a web page at the address http://ip-of-rpi:8000.
On the web page, use the range input to set the threshold for detection. Adjust the desired value so that the detector is as close as possible to the threshold where the Peak hold message is displayed.
Note: Calibration should be performed regularly to ensure the accuracy of the detector. Always follow all safety guidelines when using the detector, and use at your own risk.

## Description
Disclaimer: This police detector is provided for informational purposes only and should not be relied upon as a sole source of information. The accuracy of the information provided by this detector is not guaranteed and the use of this detector is at the user's own risk. The creator of this detector is not responsible for any errors or omissions, or for any actions taken based on the information provided by this detector. By using this detector, the user assumes all risk and responsibility for any consequences that may result from its use.