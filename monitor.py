import json
import subprocess
import time

from http.server import HTTPServer, BaseHTTPRequestHandler
#Switch to DEBUG mode
DEBUG = True

if DEBUG == True:
    FREQ_MIN = "448M"
    FREQ_MAX = "450.5M"
    STEP = "2.5M"
else:
    FREQ_MIN_C = [51, 56, 48, 77]
    FREQ_MIN = ""
    for i in FREQ_MIN_C:
        FREQ_MIN = FREQ_MIN + chr(i)
    FREQ_MIN = str(FREQ_MIN)

    FREQ_MAX_C = [51, 56, 50, 46, 53, 77]
    FREQ_MAX = ""
    for i in FREQ_MAX_C:
        FREQ_MAX = FREQ_MAX + chr(i)
    FREQ_MAX = str(FREQ_MAX)

    STEP = "2.5M"

# Running the rtl_power command
process = subprocess.Popen(['rtl_power', '-f', f"{FREQ_MIN}:{FREQ_MAX}:{STEP}", '-g', '0', '-i', 's'], stdout=subprocess.PIPE)

def get_time_and_max_power():
    # Read one line from the standard output of rtl_power
    output = process.stdout.readline().decode().strip()

    # If the line is empty, it means that the command has finished and we return None
    if not output:
        return None

    # Split the row into items and get the time and max_power values
    lines = output.split(',')
    time = lines[1]
    max_power = lines[-1]

    return time, max_power

class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/data':
            # Get data using get_time_and_max_power()
            data = get_time_and_max_power()
            if data is None:
                self.send_response(500)
                self.end_headers()
            else:
                self.send_response(200)
                self.send_header('Content-Type', 'application/json')
                self.end_headers()
                self.wfile.write(json.dumps(data).encode())
        else:
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()
            html = """
            <!DOCTYPE html>
            <html>
                <head>
                    <title>RTL Power</title>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <style>
                        body {
                            font-family: Arial, sans-serif;
                        }
                        .navbar {
                            overflow: hidden;
                            background-color: #333;
                            position: fixed;
                            top: 0;
                            width: 100%;
                        }
                        .navbar a {
                            float: left;
                            display: block;
                            color: white;
                            text-align: center;
                            padding: 14px 16px;
                            text-decoration: none;
                        }
                        .navbar a:hover {
                            background-color: #ddd;
                            color: black;
                        }
                        .navbar a.active {
                            background-color: #4CAF50;
                            color: white;
                        }
                        .navbar .right {
                            float: right;
                        }
                        .center {
                            text-align: center;
                            }
                        .canvas-container {
                            position: absolute;
                            top: 50px;
                            left: 0;
                            right: 0;
                            bottom: 0;
                        }
                        canvas {
                            width: 100%;
                            height: 100%;
                        }
                    </style>
                </head>
                <body>
                    <div class="navbar">
                        <form>
                            <label for="threshold-range">TRESHOLD:</label><br>
                            <input type="range" id="threshold-range" name="threshold" min="-40" max="5" value="-26">
                            <span id="treshold-actual" style="color: red;">Plese set treshold first!</span>
                        </form> 

                        <div class="right">
                            <span id="time"></span> | <span id="max_power"></span>
                        </div>
                        <div id="peak-detect" class="center" style="color: red;"><h1>Peak detect!</h1></div>
                    </div>
                    <br /><br /><br />
                    <div class="canvas-container">
                        <canvas id="myChart"></canvas>
                    </div>
                    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
                    <script>
                        var ctx = document.getElementById('myChart').getContext('2d');
                        var myChart = new Chart(ctx, {
                            type: 'line',
                            data: {
                                labels: [],
                                datasets: [{
                                    label: 'Max power',
                                    data: [],
                                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                                    borderColor: 'rgba(255, 99, 132, 1)',
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                        setInterval(function() {
                            var xhttp = new XMLHttpRequest();
                            xhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {
                                    var data = JSON.parse(this.responseText);
                                    myChart.data.labels.push(data[0]);
                                    myChart.data.datasets[0].data.push(data[1]);
                                    document.getElementById('time').innerHTML = data[0];
                                    document.getElementById('max_power').innerHTML = data[1];
                                    if (myChart.data.labels.length > 60) {
                                        myChart.data.labels.shift();
                                        myChart.data.datasets[0].data.shift();
                                    }
                                    myChart.update();

                                    const range = document.getElementById('threshold-range');
                                    range.addEventListener('input', (event) => {
                                        threshold = parseInt(event.target.value, 10);
                                        document.getElementById('treshold-actual').innerHTML = threshold;
                                        console.log(threshold)

                                    });
                                    
                                    if (data[1] > threshold) {
                                        document.getElementById("peak-detect").style.display = "block";
                                        console.log("Peak detected!")
                                    } else {
                                        document.getElementById("peak-detect").style.display = "none";
                                    }
                                }
                            };
                            xhttp.open("GET", "/data", true);
                            xhttp.send();
                        }, 1000);
                    </script>
                </body>
            </html>
            """
            self.wfile.write(html.encode())

server_address = ('', 8000)
httpd = HTTPServer(server_address, RequestHandler)
print('Starting server')
httpd.serve_forever()

